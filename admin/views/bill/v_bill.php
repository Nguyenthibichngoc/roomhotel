<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
        <button  style="margin:auto" class="btn btn-default" onclick="window.location.href='add_bill.php'" id="" onclick="">Thêm </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Mã hóa đơn</th>
                            <th>Tên khách hàng</th>
                            <th>Tên phòng</th>
                            <th>Ngày đến</th>
                            <th>Ngày đi</th>
                            <th>Tổng hóa đơn</th>
                            <th>Phương thức thanh toán</th>
                            <th>Mô tả</th>
                            <th>Trạng thái</th>
                            <th style="">Hành động</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination div_trang"><?php if ($count>8)
                                    {echo $lst;}
                                    ?> </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($bills as $bill)
                        {
                            ?>
                            <tr>
                                <td><?php echo $bill->id;?></td>
                                <td><?php echo $bill->full_name;?></td>
                                <td><?php echo $bill->room_name;?></td>
                                <td><?php echo $bill->arrive;?></td>
                                <td><?php echo $bill->departure;?></td>
                                <td><?php echo $bill->total_bill;?></td>
                                <td><?php echo $bill->payment_methods;?></td>
                                <td><?php echo $bill->description;?></td>
                                <td style="background-color: <?php echo $bill->status ?>">
                                    <?php echo $bill->status ? "Đã thanh toán" : "Chưa thanh toán" ?>
                                </td>
                                <td><!-- Icons -->
<!--                                    <a href="edit_bill.php?id=--><?php //echo $bill->id;?><!--" title="Edit">-->
<!--                                        <img src="public/layout/resources/images/icons/pencil.png" alt="Edit"/>-->
<!--                                    </a>-->
                                    <a href="detail_bill.php?id=<?php echo $bill->id;?>" title="Detail">
                                        <img src="public/assets/images/icon/resume.png" width="15px" height="15px" alt="Detail"/>
                                    </a>
                                    <a href="delete_bill.php?id=<?php echo $bill->id;?>" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a>

                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
