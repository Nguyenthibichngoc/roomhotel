
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='bill.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Sửa thông tin hóa đơn</h4>
                                <fieldset>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mã khách hàng</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ma_kh" name="id_customer" placeholder="Mã khách hàng" value="<?php echo $customer->id;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mã phòng</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ma_phong" name="id_room" placeholder="Mã phòng" value="<?php echo $room->id;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Ngày đến</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="ngay_den" name="arrive" placeholder="Ngày đến" value="<?php echo $bills->arrive;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Ngày đi</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="ngay_di" name="departure" placeholder="Ngày đi" value="<?php echo $bills->departure;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tổng hóa đơn</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="tong_hoa_don" name="total_bill" placeholder="Tổng hóa đơn" value="<?php echo $bills->total_bill;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Phương thức thanh toán</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="thanh_toan" name="payment_methods" placeholder="Phương thức thanh toán" value="<?php echo $bills->payment_methods;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mô tả</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="mo_ta" name="description" placeholder="Mô tả" value="<?php echo $bills->description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="status">
                                                <option>--Chọn--</option>
                                                <option value="1">Hóa đơn gốc</option>
                                                <option value="0">Hóa đơn chỉnh sửa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                        <input class="button" type="submit" value="Sửa" name="btnCapnhat" onclick="return Kiemtradulieu();" />
                                        <input class="button" type="button" value="Bỏ qua" onclick="window.location='bill.php'" />
                                    </p>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
