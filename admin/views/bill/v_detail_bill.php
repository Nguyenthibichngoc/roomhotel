
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='bill.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Chi tiết Hóa Đơn</h4>
                                <fieldset>
                                    <style>
                                        table {
                                            font-family: arial, sans-serif;
                                            border-collapse: collapse;
                                            width: 100%;
                                        }

                                        td, th {
                                            border: 1px solid #dddddd;
                                            text-align: left;
                                            padding: 8px;
                                        }
                                    </style>
                                    <table>
                                        <tr>
                                            <th>Mã hóa đơn:</th>
                                            <th><?php echo $bills->id?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên khách hàng:</th>
                                            <th><?php echo $customer->full_name?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên phòng:</th>
                                            <th><?php echo $room->room_name?></th>
                                        </tr>
                                        <tr>
                                            <th>Ngày đến:</th>
                                            <th><?php echo $bills->arrive?></th>
                                        </tr>
                                        <tr>
                                            <th>Ngày đi:</th>
                                            <th><?php echo $bills->departure?></th>
                                        </tr>
                                        <tr>
                                            <th>Tổng hóa đơn:</th>
                                            <th><?php echo $bills->total_bill?></th>
                                        </tr>
                                        <tr>
                                            <th>Phương thức thanh toán:</th>
                                            <th><?php echo $bills->payment_methods?></th>
                                        </tr>
                                        <tr>
                                            <th>Mô tả:</th>
                                            <th><?php echo $bills->description?></th>
                                        </tr>
                                        <tr>
                                            <th>Trạng thái:</th>
                                            <th style="background-color: <?php echo $bill->status ?>"><?php echo $bills->status ? "Đã thanh toán" : "Chưa thanh toán"?></th>
                                        </tr>
                                    </table>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

