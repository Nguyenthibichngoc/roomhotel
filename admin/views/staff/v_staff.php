<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
        <button  style="margin:auto" class="btn btn-default" onclick="window.location.href='add_staff.php'" id="" onclick="">Thêm </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Mã nhân viên</th>
                            <th>Mã bộ phận</th>
                            <th>Tên nhân viên</th>
                            <th>Tên đăng nhập</th>
                            <th>Mật khẩu</th>
                            <th>Ngày sinh</th>
                            <th>Giới tính</th>
                            <th>Số Cmt</th>
                            <th>Số điện thoại</th>
                            <th>Địa chỉ</th>
                            <th>Vị trí công việc</th>
                            <th>Mô tả vị trí công việc</th>
                            <th>Trạng thái</th>
                            <th style="">Hành động</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination"> <?php if ($count>8)
                                    {echo $lst;}
                                    ?>  </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($staffs as $staff) {
                            ?>
                            <tr>
                                <td><a href="edit_staff.php" title="Edit"><?php echo $staff->id;?></a></td>
                                <td><?php echo $staff->id_role ?></td>
                                <td><a href="detail_staff.php" title="Detail"><?php echo $staff->staff_name;?></td>
                                <td><?php echo $staff->username;?></td>
                                <td><?php echo $staff->password;?></td>
                                <td><?php echo $staff->birth_day;?></td>
                                <td><?php echo $staff->sex;?></td>
                                <td><?php echo $staff->passport_number;?></td>
                                <td><?php echo $staff->phone_number;?></td>
                                <td><?php echo $staff->address;?></td>
                                <td><?php echo $staff->position;?></td>
                                <td><?php echo $staff->description;?></td>
                                <td style="background-color: <?php echo $staff->status ?>;color:black;">
                                    <?php echo $staff->status ? "Đang làm việc" : "Đã thôi việc" ?>
                                </td>

                                <td><!-- Icons -->
                                    <a href="edit_staff.php? id =<?php echo $staff->id?>" title="Edit">
                                        <img src="public/layout/resources/images/icons/pencil.png" alt="Edit"/>
                                    </a>
                                    <a href="javascript:hoi_delete_staff(<?php echo $staff->staff_name;?>)" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>