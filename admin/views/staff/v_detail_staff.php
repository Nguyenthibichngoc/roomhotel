
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='staff.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Chi tiết thông tin Nhân Viên</h4>
                                <fieldset>
                                    <style>
                                        table {
                                            font-family: arial, sans-serif;
                                            border-collapse: collapse;
                                            width: 100%;
                                        }

                                        td, th {
                                            border: 1px solid #dddddd;
                                            text-align: left;
                                            padding: 8px;
                                        }
                                    </style>
                                    <table>
                                        <tr>
                                            <th>Mã nhân viên:</th>
                                            <th><?php echo $staff->id?></th>
                                        </tr>
                                        <tr>
                                            <th>Mã bộ phận:</th>
                                            <th><?php echo $staff->id_role?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên nhân viên:</th>
                                            <th><?php echo $staff->staff_name?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên đăng nhập:</th>
                                            <th><?php echo $staff->username?></th>
                                        </tr>
                                        <tr>
                                            <th>Mật khẩu:</th>
                                            <th><?php echo $staff->password?></th>
                                        </tr>
                                        <tr>
                                            <th>Ngày sinh:</th>
                                            <th><?php echo $staff->birth_day?></th>
                                        </tr>
                                        <tr>
                                            <th>Giới tính:</th>
                                            <th><?php echo $staff->sex?></th>
                                        </tr>
                                        <tr>
                                            <th>Số Cmt:</th>
                                            <th><?php echo $staff->passport_number?></th>
                                        </tr>
                                        <tr>
                                            <th>Số điện thoại:</th>
                                            <th><?php echo $staff->phone_number?></th>
                                        </tr>
                                        <tr>
                                            <th>Địa chỉ:</th>
                                            <th><?php echo $staff->address?></th>
                                        </tr>
                                        <tr>
                                            <th>Vị trí công việc:</th>
                                            <th><?php echo $staff->position?></th>
                                        </tr>
                                        <tr>
                                            <th>Mô tả vị trí công việc:</th>
                                            <th><?php echo $staff->description?></th>
                                        </tr>
                                        <tr>
                                            <th>Trạng thái:</th>
                                            <th style="background-color: <?php echo $staff->status ?>" ><?php echo $staff->status ? "Đang làm việc" : "Đã thôi việc"?></th>
                                        </tr>
                                    </table>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
