
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='staff.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Sửa thông tin Nhân Viên</h4>
                                <fieldset>

                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tên bộ phận</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="ma_bo_phan" name="id_role" >
                                                <?php foreach ($roles as $role) {
                                                    ?>
                                                    <option value="<?php echo $role->id;?>"><?php echo $role->name_role;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tên nhân viên</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ten_nhan_vien" name="staff_name" placeholder="Tên nhân viên"
                                                   value="<?php echo $staff_edit->staff_name;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tên đăng nhập</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ten_dang_nhap" name="username" placeholder="Tên đăng nhập"
                                                   value="<?php echo $staff_edit->username;?>" >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mật khẩu</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="mat_khau" name="password" placeholder="Mật khẩu"
                                                   value="<?php echo $staff_edit->password;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Ngày sinh</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="ngay_sinh" name="birth_day" placeholder="Ngày sinh"
                                                   value="<?php echo $staff_edit->birth_day;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Giới tính</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="gioi_tinh" name="sex" placeholder="Giới tính"
                                                   value="<?php echo $staff_edit->sex;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Số Cmt</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="so_cmt" name="passport_number" placeholder="Số Cmt"
                                                   value="<?php echo $staff_edit->passport_number;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Số điện thoại</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="so_dien_thoai" name="phone_number" placeholder="Số điện thoại"
                                                   value="<?php echo $staff_edit->phone_number;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Địa chỉ</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="dia_chi" name="address" placeholder="Địa chỉ"
                                                   value="<?php echo $staff_edit->address;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Vị trí công việc</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="vi_tri" name="position" placeholder="Vị trí công việc"
                                                   value="<?php echo $staff_edit->position;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mô tả vị trí công việc</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="mo_ta" name="description" placeholder="Mô tả vị trí công việc"
                                                   value="<?php echo $staff_edit->description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="status">
                                                <option>--Chọn--</option>
                                                <option value="1">Đang làm việc</option>
                                                <option value="0">Đã thôi việc</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                        <input class="button" type="submit" value="Sửa" name="btnCapnhat" onclick="return Kiemtradulieu();" />
                                        <input class="button" type="button" value="Bỏ qua" onclick="window.location='staff.php'" />
                                    </p>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

