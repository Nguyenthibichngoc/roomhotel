
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='room.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Sửa thông tin phòng</h4>
                                <fieldset>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tên phòng</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ten_phong" name="room_name" placeholder="Tên phòng" value="<?php echo $rooms->room_name;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Loại phòng</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="id" name="id_room_category"  >
                                                <?php foreach ($category as $room) {
                                                    ?>
                                                    <option value="<?php echo $room->id;?>"><?php echo $room->name;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Đơn giá</label>
                                        <div class="col-sm-9">
                                            <input type="float" class="form-control" id="don_gia" name="price" placeholder="Đơn giá" value="<?php echo $rooms->price;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Hình ảnh</label>
                                        <div class="col-sm-9">
                                            <div class="custom-file">
                                                <input type="file" name="f_picture" id="picture" />
                                                <div class="image-holder" id="image-holder"></div>
                                                <?php if ($rooms->picture != ""){?>
                                                    <img src="public/assets/images/picture_room/<?php echo $rooms->picture;?>" width="100px" />
                                                <?php }?>
                                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                                <div class="invalid-feedback">Example invalid custom file feedback</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mô tả</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="mo_ta" name="description" placeholder="Mô tả"value="<?php echo $rooms->description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="status">
                                                <option>--Chọn--</option>
                                                <option value="1">Trống</option>
                                                <option value="0">Hết</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                        <input class="button" type="submit" value="Sửa" name="btnCapnhat" onclick="return Kiemtradulieu();" />
                                        <input class="button" type="button" value="Bỏ qua" onclick="window.location='room.php'" />
                                    </p>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
