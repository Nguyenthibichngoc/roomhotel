<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin: auto" class="btn btn-default" onclick="">Quay lại </button>
        <button  style="margin:auto" class="btn btn-default" onclick="window.location.href='add_role_staff.php'" id="" onclick="">Thêm </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Mã bộ phận</th>
                            <th>Tên bộ phận</th>
                            <th style="">Hành động</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination"> <?php if ($count>8)
                                    {echo $lst;}
                                    ?>  </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($role_staff as $role) {
                            ?>
                            <tr>

                                <td><?php echo $role->id;?></td>
                                <td><a href="#" title="Edit"><?php echo $role->name_role;?></a></td>

                                <td><!-- Icons -->
                                    <a href="edit_role_staff.php? id =<?php echo $role->id?>" title="Edit">
                                        <img src="public/layout/resources/images/icons/pencil.png" alt="Edit"/>
                                    </a>
                                    <a href="javascript:delete_role_staff(<?php echo $role->id;?>)" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a>
                                </td>
                            </tr>
                            <?php

                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>