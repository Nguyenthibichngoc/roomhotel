
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='customer.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Chi tiết thông tin Khách Hàng</h4>
                                <fieldset>
                                    <style>
                                        table {
                                            font-family: arial, sans-serif;
                                            border-collapse: collapse;
                                            width: 100%;
                                        }

                                        td, th {
                                            border: 1px solid #dddddd;
                                            text-align: left;
                                            padding: 8px;
                                        }
                                    </style>
                                    <table>
                                        <tr>
                                            <th>Mã khách hàng:</th>
                                            <th><?php echo $customer->id?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên khách hàng:</th>
                                            <th><?php echo $customer->full_name?></th>
                                        </tr>
                                        <tr>
                                            <th>Địa chỉ:</th>
                                            <th><?php echo $customer->address?></th>
                                        </tr>
                                        <tr>
                                            <th>Email:</th>
                                            <th><?php echo $customer->email?></th>
                                        </tr>
                                        <tr>
                                            <th>Số điện thoại:</th>
                                            <th><?php echo $customer->phone_number?></th>
                                        </tr>
                                        <tr>
                                            <th>Trạng thái:</th>
                                            <th style="background-color: <?php echo $customer->status ?>"><?php echo $customer->status ? "Hoạt động" : "Vô hiệu hóa"?></th>
                                        </tr>
                                    </table>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

