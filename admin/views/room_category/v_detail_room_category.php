
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='room_category.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Chi tiết Loại Phòng</h4>
                                <fieldset>
                                    <style>
                                        table {
                                            font-family: arial, sans-serif;
                                            border-collapse: collapse;
                                            width: 100%;
                                        }

                                        td, th {
                                            border: 1px solid #dddddd;
                                            text-align: left;
                                            padding: 8px;
                                        }
                                    </style>
                                    <table>
                                        <tr>
                                            <th>Mã phòng:</th>
                                            <th><?php echo $room_category->id?></th>
                                        </tr>
                                        <tr>
                                            <th>Tên phòng:</th>
                                            <th><?php echo $room_category->name?></th>
                                        </tr>
                                        <tr>
                                            <th>Số lượng:</th>
                                            <th><?php echo $room_category->quantity?></th>
                                        </tr>
                                        <tr>
                                            <th>Mô tả:</th>
                                            <th><?php echo $room_category->description?></th>
                                        </tr>
                                        <tr>
                                            <th>Trạng thái:</th>
                                            <th style="background-color: <?php echo $room_category->status ?>><?php echo $room_category->status ? "Đang sử dụng" : "Đang sửa chữa"?></th>
                                        </tr>
                                    </table>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
