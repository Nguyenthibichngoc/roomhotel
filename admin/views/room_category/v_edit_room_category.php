
<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 57%;">
        <!--        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='category.php'">Quay lại </button>-->
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='room_category.php'">Quay lại </button>
    </div>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" id="" enctype="multipart/form-data" method="post" action="">
                            <div class="card-body">
                                <h4 class="card-title">Sửa thông tin loại phòng</h4>
                                <fieldset>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Tên loại phòng</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ten_loai_phong" name="name" placeholder="Tên loại phòng"value="<?php echo $room_category->name;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Số lượng</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="so_luong" name="quantity" placeholder="Số lượng"value="<?php echo $room_category->quantity;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Mô tả</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="mo_ta" name="description" placeholder="Mô tả"value="<?php echo $room_category->description;?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="status">
                                                <option>--Chọn--</option>
                                                <option value="1">Đang sử dụng</option>
                                                <option value="0">Đang sửa chữa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <p>
                                        <input class="button" type="submit" value="Sửa" name="btnCapnhat" onclick="return Kiemtradulieu();" />
                                        <input class="button" type="button" value="Bỏ qua" onclick="window.location='room_category.php'" />
                                    </p>
                                </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




