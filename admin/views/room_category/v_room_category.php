<div id="main-wrapper">
    <div class="content-heading text-center" style="margin-right: 53%">
        <button  style="margin: auto" class="btn btn-default" onclick="window.location.href='home.php'">Quay lại </button>
        <button  style="margin:auto" class="btn btn-default" onclick="window.location.href='add_room_category.php'" id="" onclick="">Thêm </button>
    </div>

    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="content-box-header">
                <h3><?php echo $tieude;?></h3>
                <div class="clear"></div>
            </div>
            <!-- End .content-box-header -->
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Mã phòng</th>
                            <th>Tên phòng</th>
                            <th>Số lượng</th>
                            <th>Mô tả</th>
                            <th>Trạng thái</th>
                            <th style="">Hành động</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <div class="pagination"> <?php if ($count>8)
                                    {echo $lst;}
                                    ?>  </div>
                                <div class="clear"></div></td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php foreach ($room_category as $room) {
                            ?>
                            <tr>

                                <td><?php echo $room->id;?></a></td>
                                <td><?php echo $room->name;?></td>
                                <td><?php echo $room->quantity;?></td>
                                <td><?php echo $room->description;?></td>
                                <td style="background-color: <?php echo $room->status ?>;color:black;">
                                    <?php echo $room->status ? "Đang sử dụng" : "Đang sửa chữa" ?>
                                </td>

                                <td><!-- Icons -->
                                    <a href="edit_room_category.php?id=<?php echo $room->id?>" title="Edit">
                                        <img src="public/layout/resources/images/icons/pencil.png" alt="Edit"/>
                                    </a>
                                    <a href="detail_room_category.php?id=<?php echo $room->id;?>" title="Detail">
                                        <img src="public/assets/images/icon/resume.png" width="15px" height="15px" alt="Detail"/>
                                    </a>
                                    <a href="delete_room_category.php?id=<?php echo $room->id;?>" title="Delete">
                                        <img src="public/layout/resources/images/icons/cross.png" alt="Delete"/>
                                    </a>
                                </td>
                            </tr>
                            <?php

                        }
                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>