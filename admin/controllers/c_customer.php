<?php
include ("models/m_customer.php");
class c_customer
{
    function show_customer()
    {
// Models
        $m_customer = new m_customer();
        $customers = $m_customer->read_customer();
        $count=count($customers);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($customers);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $customers = $m_customer->read_customer($vt,$limit);
        $view = 'views/customer/v_customer.php';
        $title = "Room Management";
        $tieude = "Rooms";
        include('templates/layout.php');
// View
    }
    function delete_customer()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_customer = new m_customer();
            $kq = $m_customer->delete_customer($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='customer.php'</script>";
            }

        }

    }
    public function show_detail_customer()
    {

        $m_customer = new m_customer();
        $customer = $m_customer->read_customer();
        $id = $customer[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $customer = $m_customer->read_customer_by_id($id);
        $view = 'views/customer/v_detail_customer.php';
        include ('templates/layout.php');
    }
}
?>