<?php

include ("models/m_role_staff.php");
class c_role_staff
{
    function show_role_staff()
    {
// Models
        $m_role_staff = new m_role_staff();
        $role_staff = $m_role_staff->read_role_staff();
        $count=count($role_staff);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($role_staff);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $role_staff = $m_role_staff->read_role_staff($vt,$limit);
        $view = 'views/role_staff/v_role_staff.php';
        $title = "Role Staff Management";
        $tieude = "Role Staff";
        include('templates/layout.php');
// View
    }
    function add_role_staff()
    {
        // Models
        $m_role_staff =new m_role_staff();
        $role_staff = $m_role_staff->read_role_staff();

        if(isset($_POST["btnCapnhat"]))
        {
            $id = $_POST["id"];
            $name_role = $_POST["name_role"];

            $m_role_staff = new m_role_staff();

            print_r($role_staff);
            foreach ($role_staff as $role)
            {
                if($id == $role->id)
                {
                    echo "<script>alert('Mã loại bị trùng thêm không thành công');window.location='add_role_staff.php'</script>";
                    return;
                }
            }
            $kq=$m_role_staff->add_role_staff($id,$name_role);
            if($kq)
            {

                echo "<script>alert('Thêm thành công');window.location='role_staff.php'</script>";

            }
            else
            {
                echo "<script>alert('Thêm không thành công')</script>";
            }

        }

        // View
        $view = 'views/role_staff/v_add_role_staff.php';
        $title = "Role Staff Management";
        $tieude = "Add Role Staff";
        include('templates/layout.php');

    }
    function edit_role_staff()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_role_staff = new m_role_staff();
            $role_staff =$m_role_staff->read_role_staff_by_id($id);
            // Cập nhật
            if(isset($_POST["btnCapnhat"]))
            {
                $name_role = $_POST["name_role"];

                $m_role_staff = new m_role_staff();

                $kq = $m_role_staff->edit_role_staff($id,$name_role);
                if($kq)
                {
                    echo "<script>alert('Cập nhật thành công');window.location='role_staff.php'</script>";
                }
                else
                {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }

            }
            // End Cập nhật



            // View
            $view = 'views/role_staff/v_edit_role_staff.php';
            $title = "Role Staff Management";
            $tieude = "Edit Role Staff";
            include('templates/layout.php');
        }


    }
    function delete_role_staff()
    {
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
            $m_role_staff = new m_role_staff();
            $kq = $m_role_staff->edit_role_staff($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='role_staff.php'</script>";

            }
        }
    }
}
?>