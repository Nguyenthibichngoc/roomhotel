<?php
include("models/m_bill.php");
include("models/m_customer.php");
include("models/m_room.php");
include("models/m_room_category.php");
class c_bill
{
    public function show_bill()
    {
        $m_bill = new m_bill();
        $bills = $m_bill->read_bill_with_customer_and_room();
        $count=count($bills);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($bills);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $bills = $m_bill->read_bill_with_customer_and_room($vt,$limit);
        $view = 'views/bill/v_bill.php';
        $title = "Bill Management";
        $tieude = "Bills";
        include('templates/layout.php');
    }
    public function add_bill()
    {
        // Models
        $m_bill = new m_bill();
        $bill = $m_bill->read_bill();

        if(isset($_POST["btnCapnhat"]))
        {
            //$id,$id_customer,$id_room,$arrive,$departure,$total_bill,$payment_methods,$description,$status
            $id = $_POST["id"];
            $id_customer = $_POST["id_customer"];
            $id_room = $_POST["id_room"];
            $arrive = $_POST["arrive"];
            $departure = $_POST['departure'];
            $total_bill = $_POST["total_bill"];
            $payment_methods = $_POST["payment_methods"];
            $description = $_POST["description"];
            $status = $_POST['status'];

            $m_bill = new m_bill();

            $kq=$m_bill->add_bill($id,$id_customer,$id_room,$arrive,$departure,$total_bill,$payment_methods,$description,$status);
            if($kq)
            {
                echo "<script>alert('Thêm thành công');window.location='bill.php'</script>";
            }
            else
            {
                echo "<script>alert('Thêm không thành công')</script>";
            }

        }

        // View
        $view = 'views/bill/v_add_bill.php';
        $title = "Bill Management";
        $tieude = "Add Bill";
        include('templates/layout.php');

    }

    public function edit_bill()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_bill = new m_bill();
            $bills = $m_bill->read_bill_by_id($id);
            $m_customer = new m_customer();
            $customer = $m_customer->read_customer_by_id($id);
            $m_room_category = new m_room_category();
            $room_category = $m_room_category->read_room_category();
            $m_room = new m_room();
            $room = $m_room->read_room_for_id($id);
            // Cập nhật
            if(isset($_POST["btnCapnhat"]))
            {

                $id_customer = $_POST["id_customer"];
                $id_room = $_POST["id_room"];
                $arrive = $_POST["arrive"];
                $departure = $_POST['departure'];
                $total_bill = $_POST["total_bill"];
                $payment_methods = $_POST["payment_methods"];
                $description = $_POST["description"];
                $status = $_POST['status'];

                $m_bill = new m_bill();

                $kq=$m_bill->edit_bill($id,$id_customer,$id_room,$arrive,$departure,$total_bill,$payment_methods,$description,$status);
                if($kq)
                {
                    echo "<script>alert('Cập nhật thành công');window.location='bill.php'</script>";
                }
                else
                {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }

            }
            // End Cập nhật
        }
        // View
        $view = 'views/bill/v_edit_bill.php';
        $title = "Bill Management";
        $tieude = "Edit Bill";
        include('templates/layout.php');
    }

    public function delete_bill()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_bill = new m_bill();
            $kq = $m_bill->delete_bill($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='bill.php'</script>";
            }

        }


    }
    public function show_detail_bill()
    {
        $m_bill = new m_bill();
        $bills = $m_bill->read_bill();
        $id = $bills[0]->id;
        $m_customer = new m_customer();
        $customer = $m_customer->read_customer_by_id($id);
        $m_room = new m_room();
        $room = $m_room->read_room_for_id($id);
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $bills = $m_bill->read_bill_by_id($id);
        $view = 'views/bill/v_detail_bill.php';
        include ('templates/layout.php');
    }
}
?>
