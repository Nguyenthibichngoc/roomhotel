<?php
include ("models/m_room.php");
include ("models/m_room_category.php");
class c_room_category
{
    function show_room_category()
    {
// Models
        $m_room_category = new m_room_category();
        $room_category = $m_room_category->read_room_category();
        $count=count($room_category);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($room_category);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $room_category = $m_room_category->read_room_category($vt,$limit);
        $view = 'views/room_category/v_room_category.php';
        $title = "Room Management Hotel";
        $tieude = "Room Category";
        include('templates/layout.php');
// View
    }
    function add_room_category()
    {
        // Models
        $m_room_category =new m_room_category();
        $room_category = $m_room_category->read_room_category();
        
        if(isset($_POST["btnCapnhat"]))
        {
            $id = $_POST["id"];
            $name = $_POST["name"];
            $quantity=$_POST["quantity"];
            $description=$_POST["description"];
            $status = $_POST["status"];

            $m_room_category = new m_room_category();

            print_r($room_category);
            foreach ($room_category as $room)
            {
                if($id == $room->id)
                {
                    echo "<script>alert('Mã loại bị trùng thêm không thành công');window.location='add_room_category.php'</script>";
                    return;
                }
            }
            $kq=$m_room_category->add_room_category($id,$name,$quantity,$description,$status);
            if($kq)
            {

                echo "<script>alert('Thêm thành công');window.location='room_category.php'</script>";

            }
            else
            {
                echo "<script>alert('Thêm không thành công')</script>";
            }

        }

        // View
        $view = 'views/room_category/v_add_room_category.php';
        $title = "Room Management Hotel";
        $tieude = "Add Room Category";
        include('templates/layout.php');

    }
    function edit_room_category()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_room_category = new m_room_category();
            $room_category = $m_room_category->read_room_category();
            $room_category = $m_room_category->read_room_category_for_id($id);
//            $m_room =new m_room();
//            $rooms  = $m_room->read_room_for_id($id);
            // Cập nhật
            if(isset($_POST["btnCapnhat"]))
            {
                $name = $_POST["name"];
                $quantity = $_POST["quantity"];
                $description = $_POST["description"];
                $status = $_POST["status"];

                $m_room_category = new m_room_category();

                $kq=$m_room_category->edit_room_category($id,$name,$quantity,$description,$status);
                if($kq)
                {
                    echo "<script>alert('Cập nhật thành công');window.location='room_category.php'</script>";
                }
                else
                {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }

            }
            // End Cập nhật

            // View
            $view = 'views/room_category/v_edit_room_category.php';
            include('templates/layout.php');
        }


    }
    public function delete_room_category()
    {
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
            $m_room_category = new m_room_category();
            $kq = $m_room_category->delete_room_category($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='room_category.php'</script>";

            }
        }
    }
    public function show_detail_room_category()
    {

        $m_room_category = new m_room_category();
        $room_category = $m_room_category->read_room_category();
        $id = $room_category[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $room_category = $m_room_category->read_room_category_for_id($id);
        $view = 'views/room_category/v_detail_room_category.php';
        include ('templates/layout.php');
    }
}
?>