<?php

include ("models/m_room.php");
include ("models/m_room_category.php");
class c_room
{
    function show_room()
    {
// Models
        $m_room = new m_room();
        $rooms = $m_room->read_room_with_room_category();
        $count=count($rooms);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($rooms);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $rooms = $m_room->read_room_with_room_category($vt,$limit);
        $view = 'views/room/v_room.php';
        $title = "Room Management";
        $tieude = "Rooms";
        include('templates/layout.php');
// View
    }
    function edit_room()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_room_category = new m_room_category();
            $category = $m_room_category->read_room_category();
            $m_room =new m_room();
            $rooms  = $m_room->read_room_for_id($id);
            // Cập nhật
            if(isset($_POST["btnCapnhat"]))
            {

                $room_name = $_POST["room_name"];
                $id_room_category = $_POST["id_room_category"];
                $price = $_POST["price"];
                $picture = $_FILES["f_picture"]["error"]==0?$_FILES["f_picture"]["name"]:$rooms->picture;
                $description = $_POST["description"];
                $status = $_POST['status'];

                $m_room = new m_room();

                $kq=$m_room->edit_room($id,$room_name,$id_room_category,$price,$picture,$description,$status);
                if($kq)
                {
                    if($_FILES["f_picture"]["error"]==0)
                    {
                        move_uploaded_file($_FILES["f_picture"]["tmp_name"],"public/assets/images/picture_room/$picture");
                    }

                    echo "<script>alert('Cập nhật thành công');window.location='room.php'</script>";
                }
                else
                {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }

            }
            // End Cập nhật

            // View
            $view = 'views/room/v_edit_room.php';
            include('templates/layout.php');
        }

    }
    function add_room()
    {
        // Models
        $room_category = new m_room_category();
        $rooms = $room_category->read_room_category();
        if(isset($_POST["btnCapnhat"]))
        {

            $id = $_POST["id"];
            $room_name = $_POST["room_name"];
            $id_room_category = $_POST["id_room_category"];
            $price = $_POST["price"];
            $picture = $_FILES["f_picture"]["error"]==0?$_FILES["f_picture"]["name"]:$rooms->picture;
            $description = $_POST["description"];
            $status = $_POST['status'];

            $m_room = new m_room();

            $kq=$m_room->add_room($id,$room_name,$id_room_category,$price,$picture,$description,$status);
            if($kq)
            {
                if($picture != "")
                {
                    move_uploaded_file($_FILES["f_picture"]["tmp_name"],"public/assets/images/picture_room/$picture");
                }
                echo "<script>alert('Thêm thành công');window.location='room.php'</script>";
            }
            else
            {
                echo "<script>alert('Thêm không thành công')</script>";
            }

        }

        // View
        $view = 'views/room/v_add_room.php';
        $title = "Room Management Hotel";
        $tieude = "Add Room";
        include('templates/layout.php');

    }
    function delete_room()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_room = new m_room();
            $kq = $m_room->delete_room($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='room.php'</script>";
            }

        }

    }
    public function show_detail_room()
    {

        $m_room = new m_room();
        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $room = $m_room->read_room_for_id($id);
        $view = 'views/room/v_detail_room.php';
        include ('templates/layout.php');
    }
}
?>