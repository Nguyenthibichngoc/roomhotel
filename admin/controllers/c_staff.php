<?php
@session_start();
include ("models/m_staff.php");
include ("models/m_role_staff.php");
class c_staff {
    public function checkLogin() {
        $flag = false;
        if(isset($_POST['login'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            //  $m_staff = new m_staff();
            $this->saveLoginToSession($username,$password);
            if (isset($_SESSION['staff_admin'])) {
                header("location:home.php");
            } else {
                $_SESSION['error_login'] = "Sai thông tin đăng nhập";
                header("location:login.php");
            }
        }
    }

    public function logout(){
        unset($_SESSION['staff_admin']);
        unset($_SESSION['error_login']);
        header("location:login.php");
    }
    public function saveLoginToSession($username,$password) {
        $m_staff = new m_staff();
        $staff = $m_staff->read_staff_by_id_password($username,$password);
        if (!empty($staff)){
            $_SESSION['staff_admin'] = $staff;
        }
    }
    public function show_staff()
    {
// Models
        $m_staff = new m_staff();
        $staffs = $m_staff->read_staff();
        $count=count($staffs);
        // Phân trang
        include("lib/Pager.php");
        $p=new Pager();
        $limit=8;
        $count=count($staffs);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $staffs = $m_staff->read_staff($vt,$limit);
        $view = 'views/staff/v_staff.php';
        $title = "Staff Management";
        $tieude = "Staffs";
        include('templates/layout.php');
// View
    }
    public function edit_staff()
    {
        // Models
        $m_staff = new m_staff();
        $staff = $m_staff->read_staff();
        $id = $staff[0]->id;
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_role_staff = new m_role_staff();
            $roles = $m_role_staff->read_role_staff();
            $m_staff = new m_staff();
            $staff_edit  = $m_staff->read_staff_by_id($id);
            // Cập nhật
            if(isset($_POST["btnCapnhat"]))
            {
                $id_role = $_POST["id_role"];
                $staff_name = $_POST["staff_name"];
                $username = $_POST["username"];
                $password = $_POST["password"];
                $birth_day = $_POST["birth_day"];
                $sex = $_POST["sex"];
                $passport_number = $_POST["passport_number"];
                $phone_number = $_POST["phone_number"];
                $address = $_POST["address"];
                $position = $_POST["position"];
                $description = $_POST["description"];
                $status = $_POST["status"];

                $m_staff = new m_staff();

                $kq=$m_staff->edit_staff($id,$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status);
                if($kq)
                {
                    echo "<script>alert('Cập nhật thành công');window.location='staff.php'</script>";
                }
                else
                {
                    echo "<script>alert('Cập nhật không thành công')</script>";
                }
            }
            // End Cập nhật
        }
        // View
        $view = 'views/staff/v_edit_staff.php';
        $title = "Staffs Management";
        $tieude = "Edit Staff";
        include('templates/layout.php');

    }
    public function add_staff()
    {
        // Models
        $staff = new m_role_staff();
        $staffs = $staff->read_role_staff();
        if(isset($_POST["btnCapnhat"]))
        {

            $id = NULL;
            $id_role = $_POST["id_role"];
            $staff_name = $_POST["staff_name"];
            $username = $_POST["username"];
            $password = $_POST['password'];
            $birth_day = $_POST['birth_day'];
            $sex = $_POST['sex'];
            $passport_number = $_POST['passport_number'];
            $phone_number = $_POST['phone_number'];
            $address = $_POST['address'];
            $position = $_POST['position'];
            $description = $_POST["description"];
            $status = 1;

            $m_staff = new m_staff();

            $kq=$m_staff->add_staff($id,$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status);
            if($kq)
            {
                echo "<script>alert('Thêm thành công');window.location='staff.php'</script>";
            }
            else
            {
                echo "<script>alert('Thêm không thành công')</script>";
            }

        }

        // View
        $view = 'views/staff/v_add_staff.php';
        $title = "Staffs Management";
        $tieude = "Add Staff";
        include('templates/layout.php');

    }
    public function delete_staff()
    {
        // Models
        if(isset($_GET["id"]))
        {
            $id=$_GET["id"];
            $m_staff =new m_staff();
            $kq = $m_staff->delete_staff($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='staff.php'</script>";
            }

        }


    }
    public function show_detail_staff()
    {

        $m_staff = new m_staff();
        $staff = $m_staff->read_staff();
        $id = $staff[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $staff = $m_staff->read_staff_by_id($id);
        $view = 'views/staff/v_detail_staff.php';
        include ('templates/layout.php');
    }
}