<?php
include_once ("database.php");
class m_staff extends database {

    public function read_staff(){
        $sql="SELECT * FROM staff";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_staff_by_id_password($username,$password) {
        $sql = "SELECT * FROM `staff` where username = ? and password = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($username,$password));
    }

    public function read_staff_by_id($id) {
        $sql = "SELECT * FROM `staff` where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function add_staff($id,$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status)
    {
        $sql ="insert into staff values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array('NULL',$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status));
    }
    public function edit_staff($id,$id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status)
    {
        $sql="update staff set id_role=?,staff_name=?,username=?,password=?,birth_day=?,sex=?,passport_number=?,phone_number=?,address=?,position=?,description=?,status=? Where id=?";
        $this->setQuery($sql);
        return $this->execute(array($id_role,$staff_name,$username,$password,$birth_day,$sex,$passport_number,$phone_number,$address,$position,$description,$status,$id));
    }


    public function delete_staff($id){

        $sql = "delete from staff where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}