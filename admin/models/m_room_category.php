<?php
include_once ("database.php");
class m_room_category extends database
{
    public function read_room_category($vt = -1, $limit = -1){
        $sql = "SELECT * FROM `room_category`";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();

    }
    public function read_room_category_for_id($id)
    {
        $sql = "SELECT * FROM `room_category` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function add_room_category($id,$name,$quantity,$description,$status)
    {
        $sql ="insert into room_category values(?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$name,$quantity,$description,$status));
    }
    public function edit_room_category($id,$name,$quantity,$description,$status)
    {
        $sql="update room_category set name =?,quantity=?,description=?,status=?  Where id=?";
        $this->setQuery($sql);
        return $this->execute(array($name,$quantity,$description,$status,$id));
    }


    public function delete_room_category($id){

        $sql = "delete from room_category where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
?>