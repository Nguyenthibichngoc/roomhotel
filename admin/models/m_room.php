<?php
include_once ("database.php");
class m_room extends database
{
    public function read_room($vt = -1, $limit = -1){
        $sql = "SELECT * FROM `room` ";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();

    }
    public function read_room_with_room_category($vt = -1, $limit = -1)
    {
        $sql='SELECT room.id,room.room_name,room_category.name,room.price,room.picture,room.description,room.status FROM room,room_category WHERE room.id_room_category = room_category.id';
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_room_for_id($id)
    {
        $sql = "SELECT * FROM `room` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function  add_room($id,$room_name,$id_room_category,$price,$picture,$description,$status)
    {
        $sql ="insert into room values(?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$room_name,$id_room_category,$price,$picture,$description,$status));
    }
    public function edit_room($id,$room_name,$id_room_category,$price,$picture,$description,$status)
    {
        $sql="update room set room_name = ?,id_room_category = ?,price = ?,picture = ?,description = ?,status = ? Where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($room_name,$id_room_category,$price,$picture,$description,$status,$id));
    }
    public function delete_room($id)
    {
        $sql = "DELETE FROM `room` WHERE id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
?>