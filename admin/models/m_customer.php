<?php
include_once ("database.php");
class m_customer extends database
{
    public function read_customer($vt = -1, $limit = -1){
        $sql = "SELECT * FROM `customer`";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_customer_by_id($id)
    {
        $sql = "SELECT * FROM `customer` WHERE id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function add_customer($id,$full_name,$address,$email,$phone_number,$status)
    {
        $sql ="insert into customer values(?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$full_name,$address,$email,$phone_number,$status));
    }
    public function edit_customer($id,$full_name,$address,$email,$phone_number,$status)
    {
        $sql="update bill set full_name=?,address=?,email=?,phone_number=?,status=?  Where id=?";
        $this->setQuery($sql);
        return $this->execute(array($full_name,$address,$email,$phone_number,$status,$id));
    }

    public function delete_customer($id){

        $sql = "delete from customer where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
?>
