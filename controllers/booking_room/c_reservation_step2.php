<?php
class c_reservation_step2{
    public function __construct()
    {
    }

    public  function show_room()
    {
        include ("models/m_room.php");

        $m_room = new m_room();
        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
        }
        include ("models/m_reservation_step.php");
        $rooms = $m_room ->read_room_by_idroom($id);
         if(isset($_POST['btnBook'])) {
             $full_name=$_POST['full_name'];
             $address =$_POST['address'];
             $email = $_POST["email"];
             $phone_number=$_POST['phone_number'];
             $status=1;
             $m_customer=new m_reservation_step();
             $id_customer= $m_customer->add_customer($full_name,$address,$email,$phone_number,$status);
             if($id_customer>0)
             {
//                 $id_customer, $id_room, $arrive, $departure,$total_bill,$payment_methods,$description,$status
                 $id_room=$id;
                 $date_arrive = date_create($_POST["arrive"]);
                 $arrive = date_format($date_arrive, "Y-m-d");
                 $date_departure = date_create($_POST["departure"]);
                 $departure = date_format($date_departure, "Y-m-d");
                 $datetime1 = strtotime($arrive);
                 $datetime2 = strtotime($departure);
                 $secs = $datetime2 - $datetime1;// == <seconds between the two times>
                 $days = $secs / 86400;
                 $total_bill=$rooms->price*$days;
                 $description=$_POST['description'];
                 $payment_methods = "Tiền mặt";
                 $status =1;
                 $bill=$m_customer->add_bill($id_customer, $id_room, $arrive, $departure,$total_bill,$payment_methods,$description,$status);

                 if($bill)
                 {

                     echo "<script>alert('Đặt phòng thành công!');window.location='room.php'</script>";

                 }
                 else
                 {
                     echo "<script>alert('Đặt phòng không thành công!')</script>";
                 }
             }
         }

        $view = 'views/booking_room/reservation_step2/v_reservation_step2.php';
        include("templates/frontend/layout.php");

    }

}
?>
