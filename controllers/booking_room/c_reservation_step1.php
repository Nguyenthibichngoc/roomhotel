<?php
class c_reservation_step1{
    public function __construct()
    {
    }

    public function show()
    {
        $isPaging = true;
        include ("models/m_room.php");
        include ("models/m_category.php");

        $m_room = new m_room();

        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $room = $m_room->read_room_by_category($id);
        $count=count($room);
        // Phân trang 1
        include("libs/Pager.php");
        $p=new pager();
        $limit=6;
        $count=count($room);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $room=$m_room->read_room_by_category($id,$vt,$limit);

        if (isset($_POST["btnCheck"]))
        {
            $isPaging = false;
            $date_arrive = date_create($_POST["arrive"]);
            $arrive = date_format($date_arrive, "Y-m-d");
            $date_departure = date_create($_POST["departure"]);
            $departure = date_format($date_departure, "Y-m-d");
            $id_room_category = $_POST["id_room_category"];
//            echo $id_room_category;
//            die();
            $room= array($m_room->read_room_by_idroom($id));
            $room_full = $m_room->read_room_by_category_full($id_room_category);
            $view = 'views/booking_room/reservation_step1/v_reservation_step1.php';
            include("templates/frontend/layout.php");
            return;
        }
        $view = 'views/booking_room/reservation_step1/v_reservation_step1.php';
        include("templates/frontend/layout.php");
    }






}

?>


