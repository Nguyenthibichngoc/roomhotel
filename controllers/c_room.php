<?php
@session_start();
class c_room{

    public function show()
    {
        $isPaging = true;
        include ("models/m_room.php");
        include ("models/m_category.php");

        $m_room = new m_room();

        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        $room = $m_room->read_room_by_category($id);
        $count=count($room);
        // Phân trang 1
        include("libs/Pager.php");
        $p=new pager();
        $limit=6;
        $count=count($room);
        $pages=$p->findPages($count,$limit);
        $vt=$p->findStart($limit);
        $curpage=$_GET["page"];
        $lst=$p->pageList($curpage,$pages);
        $room=$m_room->read_room_by_category($id,$vt,$limit);
        $room_full = $m_room->read_room_by_category_full($id_room_category);
        if (isset($_POST["btnTimPhong"]))
        {
            $isPaging = false;
            $room_name = $_POST["room_name"];
            $room= array($m_room->read_room_by_idroom($id));
            $room_full = $m_room->read_room_by_category_full($id_room_category);
            $view = 'views/room/v_room.php';
            include 'templates/frontend/layout.php';
            return;
        }
        $view = 'views/room/v_room.php';
        include 'templates/frontend/layout.php';

        //  $view = 'view/products/'
    }
    public  function show_detail_room()
    {
        include ("models/m_room.php");
//        include ("models/m_order.php");
//        include ("models/m_rate.php");
        $m_room = new m_room();
//        $m_rate = new M_rate();
        $room = $m_room->read_room();
        $id = $room[0]->id;
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
        }
//        $m_order = new M_oder();

        $room = $m_room ->read_room_by_idroom($id);
//        $classs = $m_couse->read_class_and_teacher_by_idcouse($ma_khoa_hoc);
//        $rates = $m_rate->Read_rate_home_by_idcouse($ma_khoa_hoc);
        $view = 'views/detail_room/v_detail.php';
        include('templates/frontend/layout.php');
    }

    public function show_home()
    {
        include ("models/m_room.php");
//        include ("models/m_news.php");
        //    include ("models/m_rate.php");
//        include ("models/m_rate.php");
        // include ("models/m_news.php");
        //    include ("models/m_rate.php");
//        $m_rate = new M_rate();
        $m_room = new m_room();
//        $m_news = new M_news();
//        $rates = $m_rate->Read_rate_home();
//        $new_couses = $m_couse->Read_couse_new(0,3);
//        $cc = $m_couse->count_couse();
//        $ct1 = $m_couse->count_teacher();

//        $cs = $m_couse->count_students();
//        $news = $m_news->read_news_by_latesthome();
        $view = 'views/home/v_home.php';
        include 'templates/frontend/layout.php';

    }

    public function search_room(){
        include ("models/m_room.php");
        $search = new m_room();
        $s = $search ->search_room();
        if (isset($_POST['btnTimphong'])){
            $s = $_POST['s'];
        }
        $view = 'views/home/v_home.php';
        include 'templates/frontend/layout.php';
    }
}
?>