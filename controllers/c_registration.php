<?php
include ("models/m_registration.php");
class c_registration{

    function add_user()
    {
        $error_1 = [];
        $m_registration = new m_registration();
//        $id,$email,$full_name,$password,$phone_number,$avatar,$status,$id_role
        if (isset($_POST["btnSave"])) {
            $id = NULL;
            $email = $_POST["email"];
            $full_name = $_POST["full_name"];
            $password = ($_POST["password"]);
            $confirm_password = ($_POST["confirm_password"]);
            $phone_number = $_POST["phone_number"];
            $avatar = "";
            $status = 1;
            $id_role = 2;
            $kq_1 = $m_registration->return_user_by_email($email);
            if($kq_1->KQ == 1)
            {
                $error_1[] = "Email đã tồn tại";
            }


            if($password!=$confirm_password)
            {
                $error_1[] = "Mật khẩu và xác nhận mật khẩu phải khớp với nhau";
            }


            if(empty($error_1)) {
                $kq = $m_registration->add_user($id, $email, $full_name, $password, $phone_number, $avatar, $status, $id_role);

                if ($kq) {
                    $success[] = "Đăng kí tài khoản thành công";
                    header("location:room.php");
                } else {
                    echo "<script>alert('Đăng kí không thành công')</script>";
                }
            }
        }
        // View
        $view = 'views/registration/v_registration.php';
        include('templates/registration/layout.php');

    }



}
?>