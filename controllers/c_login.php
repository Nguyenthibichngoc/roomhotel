<?php
@session_start();
include ("models/m_login.php");
class c_login{

    public function checkLogin(){
        $flag=false;
        if(isset($_POST['login'])) {
            $full_name = $_POST['full_name'];
            $password = $_POST['password'];
            //  $m_staff = new m_staff();
            $this->saveLoginToSession($full_name,$password);
            if (isset($_SESSION['user_login'])) {
                header("location:index.php");
            } else {
                $_SESSION['error_login'] = "Sai thông tin đăng nhập";
                header("location:login.php");
            }
        }
    }

    public function logout() {
        unset($_SESSION['user_login']);
        unset($_SESSION['error_login']);
        header("location:login.php");
    }

    public function saveLoginToSession($full_name,$password){
        $m_user = new m_login();
        $user = $m_user->read_user_by_id_password($full_name,$password);
        if(!empty($user)){
        $_SESSION['user_login']=$user;
    }
    }

}
?>

