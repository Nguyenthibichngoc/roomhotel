<?php
require_once ("database.php");
class m_room extends database
{
    public function read_room()
    {
        $sql = "select * from room";
        $this->setQuery($sql);
        return $this->loadAllRows();

    }

        public function search_room()
    {
        $sql = "SELECT * FROM `room` WHERE room_name LIKE '%s%'";
        $this->setQuery($sql);
        return $this->loadAllRows();

    }

    public function read_room_by_category($id_room_category, $vt = -1, $limit = -1)
    {
        $sql = "select * from room where id_room_category = ?";
        if ($vt >= 0 && $limit > 0) {
            $sql .= " limit $vt,$limit";
        }
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_room_category));
    }

    public function read_room_by_category_full($id_room_category)
    {
        $sql = "select * from room where id_room_category = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_room_category));
    }

    public function read_room_by_idroom($id)
    {
        $sql = "select * from room where id  = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
}

?>