<?php
require_once ("database.php");
class m_reservation_step extends database {
    public function  read_room_category(){
        $sql="SELECT * FROM `room_category`";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }


    function add_customer($full_name,$address,$email,$phone_number,$status) {
        $sql = "INSERT INTO customer(full_name,address,email,phone_number,status) ";
        $sql.= "VALUES(?,?,?,?,?)";
        $this->setQuery($sql);
        $result = $this->execute(array($full_name,$address,$email,$phone_number,$status));
        if($result)
            return $this->getLastId();  //If query execute successful, the system will return lastID in table khach_hang
        else

            return false;
    }

    function add_bill($id_customer, $id_room, $arrive, $departure,$total_bill,$payment_methods,$description,$status) {
        $sql = "INSERT INTO bill(id_customer, id_room, arrive, departure,total_bill,payment_methods,description,status) VALUES(?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        $result = $this->execute(array($id_customer, $id_room, $arrive, $departure,$total_bill,$payment_methods,$description,$status));
        if($result)
            return $this->getLastId();
        else
            return false;
    }
}


