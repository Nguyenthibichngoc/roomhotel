<!--[if IE 7]> <body class="ie7 lt-ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 8]> <body class="ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]> <body class="ie9 lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

<!-- PRELOADER -->
<div id="preloader">
    <span class="preloader-dot"></span>
</div>
<!-- END / PRELOADER -->

<!-- PAGE WRAP -->
<div id="page-wrap">

    <!-- HEADER -->
    <header id="header">

        <!-- HEADER TOP -->
        <div class="header_top">
            <div class="container">
                <div class="header_left float-left">
                    <span><i class="lotus-icon-cloud"></i> 18 °C</span>
                    <span><i class="lotus-icon-location"></i> 225 Beach Street, Australian</span>
                    <span><i class="lotus-icon-phone"></i> 1-548-854-8898</span>
                </div>
                <div class="header_right float-right">

                    <div class="dropdown currency">
                        <span>USD <i class="fa fa"></i></span>
                        <ul>
                            <li class="active"><a href="#">USD</a></li>
                            <li><a href="#">EUR</a></li>
                        </ul>
                    </div>

                    <div class="dropdown language">
                        <span>ENG</span>

                        <ul>
                            <li class="active"><a href="#">ENG</a></li>
                            <li><a href="#">FR</a></li>
                        </ul>
                    </div>
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
<!--                    <li class="nav-item dropdown fa-users">-->
<!--                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="public/layout/images/users/1.jpg" alt="user" class="rounded-circle" width="31"></a>-->
<!--                        <div class="dropdown-menu dropdown-menu-right user-dd animated">-->
<!--                            <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>-->
<!--                            <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>-->
<!--                            <div class="dropdown-divider"></div>-->
<!--                            <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>-->
<!--                            <div class="dropdown-divider"></div>-->
<!--                            <a class="dropdown-item" href="logout.php?func=exit"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>-->
<!--                            <div class="dropdown-divider"></div>-->
<!--                        </div>-->
<!--                    </li>-->
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->

                </div>
            </div>
        </div>
        <!-- END / HEADER TOP -->

        <!-- HEADER LOGO & MENU -->
        <div class="header_content" id="header_content">

            <div class="container">
                <!-- HEADER LOGO -->
                <div class="header_logo">
                    <a href="index.php"><img src="public/layout/images/logo-header.png" alt=""></a>
                </div>
                <!-- END / HEADER LOGO -->

                <!-- HEADER MENU -->
                <nav class="header_menu">
                    <ul class="menu">
                        <li >
                            <a href="index.php">Home </a>
                        </li>
                        <li><a href="">About</a></li>
                        <?php
                        @session_start();
                        require_once ("libs/Helper.php");
                        $category = Helper::loadMenu();
                        ?>
                        <li><a href="#">ROOM CATEGORY <span class="fa fa-caret-down"></a>
                            <ul class="sub-menu">
                                <?php
                                foreach ($category as $ct) {
                                    ?>
                                    <li><a href="room.php?id=<?php echo $ct->id;?>"><?php echo $ct->name;?></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
<!--                        <li>-->
<!--                            <a href="#">Room <span class="fa fa-caret-down"></span></a>-->
<!---->
<!--                            <ul class="sub-menu">-->
<!--                                <li><a href="room1.php">Standard</a></li>-->
<!--                                <li><a href="room2.php">Superior</a></li>-->
<!--                                <li><a href="room3.php">Deluxe</a></li>-->
<!--                                <li><a href="room4.php">Suite</a></li>-->
<!--                                <li><a href="room5.php">President</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
                        <li>
                            <a href="#">Restaurant <span class="fa fa-caret-down"></span></a>
                            <ul class="sub-menu">
                                <li><a href="restaurants-1.html">Restaurant 1</a></li>
                                <li><a href="restaurants-2.html">Restaurant 2</a></li>
                                <li><a href="restaurants-3.html">Restaurant 3</a></li>
                                <li><a href="restaurants-4.html">Restaurant 4</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="reservation_step1.php">Booking Room <span class="fa fa-caret-down"></span></a>
                            <ul class="sub-menu">
                                <li><a href="reservation_step1.php"> Step 1: Choose Room</a></li>
                                <li><a href="reservation_step2.php"> Step 2: Booking</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Page <span class="fa fa-caret-down"></span></a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#">Guest Book <span class="fa fa-caret-right"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="guest-book.html">Guest Book 1</a></li>
                                        <li><a href="guest-book-2.html">Guest Book 2</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Event <span class="fa fa-caret-right"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="events.html">Events</a></li>
                                        <li><a href="events-fullwidth.html">Events Fullwidth</a></li>
                                        <li><a href="events-detail.html">Events Detail</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="attractions.html">Attractions</a>
                                </li>
                                <li>
                                    <a href="#">Term Condition <span class="fa fa-caret-right"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="term-condition.html">Term Condition 1</a></li>
                                        <li><a href="term-condition-2.html">Term Condition 2</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Activiti <span class="fa fa-caret-down"></span></a>
                                    <ul class="sub-menu">
                                        <li><a href="activiti.html">Activiti</a></li>
                                        <li><a href="activiti-detail.html">Activiti Detail</a></li>
                                    </ul>
                                </li>
                                <li><a href="check-out.html">Check Out</a></li>
                                <li><a href="shortcode.html">ShortCode</a></li>
                                <li><a href="page-404.html">404 Page</a></li>
                                <li><a href="comingsoon.html">Comming Soon</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Gallery <span class="fa fa-caret-down"></span></a>
                            <ul class="sub-menu">
                                <li><a href="gallery.html">Gallery Style 1</a></li>
                                <li><a href="gallery-2.html">Gallery Style 2</a></li>
                                <li><a href="gallery-3.html">Gallery Style 3</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Blog <span class="fa fa-caret-down"></span></a>
                            <ul class="sub-menu">
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="blog-detail.html">Blog Detail</a></li>
                                <li><a href="blog-detail-fullwidth.html">Blog Detail Fullwidth</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
                <!-- END / HEADER MENU -->

                <!-- MENU BAR -->
                <span class="menu-bars">
                        <span></span>
                    </span>
                <!-- END / MENU BAR -->

            </div>
        </div>
        <!-- END / HEADER LOGO & MENU -->

    </header>
    <!-- END / HEADER -->
