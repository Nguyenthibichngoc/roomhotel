<!-- SUB BANNER -->
<section class="section-sub-banner bg-9">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>ROOMS &amp; RATES</h2>
                <p>Lorem Ipsum is simply dummy text</p>
            </div>
        </div>
    </div>
</section>
<!-- END / SUB BANNER -->

<div class="courses-page-area3">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <img style="width: 848px;height: 423px;" src="admin/public/assets/images/picture_room/<?php echo $room->picture;?>" class="img-responsive" alt="course">
                        <div class="course-details-inner">
                            <h2 class="title-default-left title-bar-high"><?php echo $room->room_name?></h2>
                            <p><?php echo $room->description?></p>
                            <h3 class="sidebar-title">Thông tin về phòng</h3>
                            <ul class="course-feature">
                                <li>Kích thước: </li>
                                <li>Số người: </li>
                                <li>Dịch vu: </li>
                                <li>Giá: <?php echo number_format($room->price);?>/Day </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="bot">
                    <a href="reservation_step2.php" class="awe-btn awe-btn-13">Booking</a>
                </div>
                </div>


            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {
        console.log(123);
    })


    $(".register-room-order").click(function() {
        const isLogged = $("#is-logged").val();
        if(isLogged == "none") {
            $("#login-button").addClass("open");
            $("#login-form").css("display", "block");
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#login-form").offset().top
            }, 500);
        }
        else {
            var href = $(this).data("href");
            window.location.href = href;
        }
    });
</script>

