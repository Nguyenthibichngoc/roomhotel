<!-- SUB BANNER -->
<section class="section-sub-banner bg-9">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>ROOMS &amp; RATES</h2>
                <p>Lorem Ipsum is simply dummy text</p>
            </div>
        </div>
    </div>
</section>
<!-- END / SUB BANNER -->
<!-- Courses Page 1 Area Start Here -->
<div class="courses-page-area1">
    <div class="container">
        <div class="row">
            <!-- RESERVATION -->
            <section class="section-reservation-page bg-white">

                <div class="container">
                    <div class="reservation-page">


                        <div class="row">

                            <div class="col-md-4 col-lg-3">

                                <div class="reservation-sidebar">

                                    <!-- SIDEBAR AVAILBBILITY -->
                                    <div class="reservation-sidebar_availability bg-gray">
                                        <div>

                                        <!-- HEADING -->
                                        <h2 class="reservation-heading">Rooms</h2>
                                        <!-- END / HEADING -->

                                        <h6 class="check_availability_title">SEARCH</h6>
                                        <div class="mailchimp-form">
                                            <form action="#" method="POST">
                                                <input style="margin-left: 25px" type="text" name="s" placeholder="Search room" class="input-text">
                                                <button style="margin-top: 15px" class="awe-btn awe-btn-13" name="btnTimPhong">Tìm kiếm</button>
                                            </form>
                                        </div>
                                        </div>

                                        <h6 class="check_availability_title">Danh mục phòng</h6>

                                        <div class="check_availability-field">
                                            <!-- UPCOMING EVENTS -->
                                            <div class="widget widget_upcoming_events">
                                                <?php foreach ($category as $ct){?>
                                                <ul>

                                                        <div class="text">
                                                            <b><h7 size="font-family:courier"> <a  href="room.php?id=<?php echo $ct->id;?>"><?php echo $ct->name;?> </a></h7></b>
                                                        </div>

                                                </ul>
                                                    <?php
                                                }?>

                                            </div>
                                            <!-- END / UPCOMING EVENTS -->
                                        </div>


<!--                                        <button class="awe-btn awe-btn-13">CHECK AVAILABLE</button>-->

                                    </div>
                                    <!-- END / SIDEBAR AVAILBBILITY -->

                                </div>

                            </div>

                            <div class="col-md-8 col-lg-9">
                                <div class="reservation_content bg-gray">
                                    <h2 class="reservation-heading">Phòng</h2>
                                        <div class="row">
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="gried-view">
                                                    <?php
                                                    if(count($room) == 0)
                                                    {
                                                        echo "<div class='alert alert-info' style='margin-left: 40px; margin-right: 30px' > Hiện vẫn chưa có  phong trong mục này. Nhấn vào <a class='thongbao' href='index.php'>đây</a> để quay lại trang chủ</div>";
                                                    }
                                                    else{
                                                        for($i = 0;$i<count($room);$i+=3) {
                                                            ?>
                                                            <?php
                                                            for ($j = $i; $j < $i + 3; $j++) {
                                                                ?>
                                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                                                                    <div class="courses-box1">
                                                                        <div class="single-item-wrapper" style="margin-left: 10px">
                                                                            <div class="courses-img-wrapper hvr-bounce-to-bottom">
                                                                                <img class="img-responsive" style="width: 250px;height: 190px;" src="admin/public/assets/images/picture_room/<?php echo $room[$j]->picture;?>" alt="room">
                                                                            </div>
                                                                            <div class="courses-content-wrapper">
                                                                                <b> <h3 class="item-title"><a><?php echo $room[$j]->room_name;?></a></h3></b>
                                                                                <h class="item-title">
                                                                                    <b>Giá:</b>
                                                                                    <?php echo number_format($room[$j]->price); ?> /Day
                                                                                </h>
                                                                                <div style="margin-left: 13px">
                                                                                    <a  class="awe-btn awe-btn-13" href="reservation_step2.php?id=<?php echo $room[$j]->id; ?>&id_room_category=<?php echo $room[$j]->id_room_category;?>">BOOK</a>
                                                                                    <a  class="awe-btn awe-btn-13" href="room_detail.php?id=<?php echo $room[$j]->id; ?>&id_room_category=<?php echo $room[$j]->id_room_category;?>">VIEW</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($j == count($room) - 1) {
                                                                    break;
                                                                }
                                                            }
                                                            ?>

                                                            <div class="clearfix"></div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <ul class="pagination-left">
                                                    <?php
                                                    if($count > 6 && $isPaging)
                                                    {
                                                        echo $lst;
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </section>
            <!-- END / RESERVATION -->

        </div>
    </div>
</div>

<!-- Courses Page 1 Area End Here -->


