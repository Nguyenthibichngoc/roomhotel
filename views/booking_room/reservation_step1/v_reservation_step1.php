<!-- SUB BANNER -->
<section class="section-sub-banner bg-16">

    <div class="awe-overlay"></div>

    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2>RESERVATION</h2>
                <p>Lorem Ipsum is simply dummy text of the printing</p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- RESERVATION -->
<!-- Courses Page 1 Area Start Here -->
<div class="courses-page-area1">
    <div class="container">
        <div class="row">
            <!-- RESERVATION -->
            <section class="section-reservation-page bg-white">

                <div class="container">
                    <div class="reservation-page">


                        <div class="row">

                            <div class="col-md-4 col-lg-3">

                                <div class="reservation-sidebar">

                                    <!-- SIDEBAR AVAILBBILITY -->
                                    <div class="reservation-sidebar_availability bg-gray">

                                        <!-- HEADING -->
                                        <h2 class="reservation-heading">Rooms</h2>
                                        <!-- END / HEADING -->
                                        <form method="POST" action="" id="checkout-form">
                                            <div class="check_availability-field">
                                                <label>Arrive</label>
                                                <input type="text" name="arrive" class="awe-calendar awe-input from" placeholder="Arrive">
                                            </div>

                                            <div class="check_availability-field">
                                                <label>Departure</label>
                                                <input type="text" name="departure" class="awe-calendar awe-input to" placeholder="Departure">
                                            </div>
    <!--                                        Danh mục phòng-->
<!--                                            <div class="check_availability-field">-->
<!--                                                <div class="widget widget_upcoming_events">-->
<!--                                                    --><?php //foreach ($category as $ct){?>
<!--                                                        <ul>-->
<!---->
<!--                                                            <div class="text">-->
<!--                                                                <b><h7 size="font-family:courier"> <a  href="reservation_step1.php?id=--><?php //echo $ct->id;?><!--">--><?php //echo $ct->name;?><!-- </a></h7></b>-->
<!--                                                            </div>-->
<!---->
<!--                                                        </ul>-->
<!--                                                        --><?php
//                                                    }?>
<!---->
<!--                                                </div>-->
<!--                                                -->
<!--                                            </div>-->
                                            <!-- END / UPCOMING EVENTS -->

                                            <div class="check_availability-field">
                                                <label>Phòng</label>
                                                <select class='awe-select' name="id_room_category">
                                                    <?php foreach ($category as $ct){?>
                                                        <option value="<?php echo $ct->id;?>" ><?php echo $ct->name;?></option>

                                                        <?php
                                                    }?>

                                                </select>

                                            </div>
                                            <div>
                                                <button class="awe-btn awe-btn-13" type="submit" name="btnCheck" value="">CHECK</button>
<!--                                                <a href="#" class="awe-btn awe-btn-13"type="submit" name="btnCheck" value="">CHECK</a>-->
                                            </div>
                                        </form>
                                    </div>
                                    <!-- END / SIDEBAR AVAILBBILITY -->

                                </div>

                            </div>

                            <div class="col-md-8 col-lg-9">
                                <br class="reservation_content bg-gray">
                                    <!-- STEP -->
                                    <?php include("v_reservation_step.php") ?>
                                    <!-- END / STEP -->

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="courses-page-top-area">
                                                    <div class="courses-page-top-left">
                                                        <p></p>
                                                    </div>
                                                    <div class="courses-page-top-right">
                                                        <ul>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="row">
                                        <!-- Tab panes -->

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="gried-view">
                                                <?php
                                                foreach ($room as $room_show){
                                                if(count($room) == 0)
                                                {
                                                    echo "<div class='alert alert-info' style='margin-left: 40px; margin-right: 30px'>Hiện vẫn chưa có  phong trong mục này. Nhấn vào <a class='thongbao' href='index.php'>đây</a> để quay lại trang chủ</div>";
                                                }
                                                else{
                                                ?>
                                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                                    <div class="courses-box1">
                                                        <div class="single-item-wrapper" style="margin-left: 10px">
                                                            <div class="courses-img-wrapper hvr-bounce-to-bottom">
                                                                <img class="img-responsive" style="    width: 250px;height: 150px;" src="admin/public/assets/images/picture_room/<?php echo $room_show->picture;?>" alt="room">
                                                            </div>
                                                            <div class="courses-content-wrapper">
                                                                <h3  class="reservation-room_name"><a><?php echo $room_show->room_name;?></a></h3>
                                                                <h class="item-title">
                                                                    <b>Giá:</b>
                                                                    <?php echo number_format($room_show->price); ?> /Day
                                                                </h>
                                                                <div style="margin-left: 13px">
                                                                    <a  class="awe-btn awe-btn-13" href="reservation_step2.php?id=<?php echo $room_show->id; ?>&id_room_category=<?php echo $room_show->id_room_category;?>">BOOK</a>
                                                                    <a  class="awe-btn awe-btn-13" href="room_detail.php?id=<?php echo $room_show->id; ?>&id_room_category=<?php echo $room_show->id_room_category;?>">VIEW</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>

                                            </div>
                                            <?php
                                            }
                                            }
                                            ?>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <ul class="pagination-left">
                                                <?php
                                                if($count > 6 && $isPaging)
                                                {
                                                    echo $lst;
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </section>
            <!-- END / RESERVATION -->

        </div>
    </div>
</div>

<!-- Courses Page 1 Area End Here -->


