<!-- ACCOMD ODATIONS -->
<section class="section-accomd awe-parallax bg-14">
    <div class="container">
        <div class="accomd-modations">
            <div class="row">
                <div class="col-md-12">
                    <div class="accomd-modations-header">
                        <h2 class="heading">ROOMS & RATES</h2>
                        <img src="public/layout/images/icon-accmod.png" alt="icon">
                        <p>Semper ac dolor vitae accumsan. Cras interdum hendrerit lacinia. Phasellus accumsan urna vitae molestie interdum. Nam sed placerat libero, non eleifend dolor.</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="accomd-modations-content owl-single">
                        <?php foreach ( $room_danh_muc as $key=> $value){?>
                        <div class="row">

                            <!-- ITEM -->
                            <div class="col-xs-4">
                                <div class="accomd-modations-room">
                                    <div class="img">
                                        <a href="#"><img src="public/layout/images/room/img-1.jpg" alt=""></a>
                                    </div>
                                    <div class="text">
                                        <h2><a href="#"><?php echo $value->name?></a></h2>

                                    </div>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- END / ACCOMD ODATIONS -->