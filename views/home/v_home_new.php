<!-- HOME NEW -->
<section class="section-event-news bg-white">
    <div class="container">

        <div class="event-news">
            <div class="row">

                <!-- EVENT -->
                <div class="col-md-6">
                    <div class="event">
                        <h2 class="heading">EVENT &amp; DEAL</h2>
                        <span class="box-border w350"></span>

                        <div class="row">

                            <!-- ITEM -->
                            <div class="col-xs-12 col-sm-12">
                                <div class="event-slide owl-single">

                                    <div class="event-item">
                                        <div class="img hover-zoom">
                                            <a href="#">
                                                <img src="public/layout/images/home/eventdeal/img-1.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="event-item">
                                        <div class="img hover-zoom">
                                            <a href="#">
                                                <img src="public/layout/images/home/eventdeal/img-1.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- END / ITEM -->

                            <!-- ITEM -->
                            <div class="col-xs-6">
                                <div class="event-item">
                                    <div class="img">
                                        <a class="hover-zoom" href="#">
                                            <img src="public/layout/images/home/eventdeal/img-2.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="text">
                                        <div class="text-cn">
                                            <h2>SAVE THE DATE</h2>
                                            <span>BECCA &amp; ROBERT</span>
                                            <a href="#" class="awe-btn awe-btn-12">VIEW MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END / ITEM -->

                            <!-- ITEM -->
                            <div class="col-xs-6">
                                <div class="event-item">
                                    <div class="img">
                                        <a class="hover-zoom" href="#">
                                            <img src="public/layout/images/home/eventdeal/img-3.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="text">
                                        <div class="text-cn">
                                            <h2>GO ON BEACH. lotus </h2>
                                            <a href="#" class="awe-btn awe-btn-12">VIEW MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END / ITEM -->

                        </div>
                    </div>
                </div>
                <!-- END / EVENT -->

                <!-- NEWS -->
                <div class="col-md-6">
                    <div class="news">
                        <h2 class="heading">NEWS</h2>
                        <span class="box-border w350 space-5"></span>

                        <div class="row">

                            <!-- ITEM -->
                            <div class="col-md-12">
                                <div class="news-item">
                                    <div class="img">
                                        <a href="#"><img src="public/layout/images/home/lotusnews/img-1.jpg" alt=""></a>
                                    </div>
                                    <div class="text">
                                        <span class="date">21 / March</span>
                                        <h2><a href="#">UPDATE MENU FOOD IN lotus HOTEL</a></h2>
                                        <a href="#" class="read-more">[ Read More ]</a>
                                    </div>
                                </div>
                            </div>
                            <!-- END / ITEM -->

                            <!-- ITEM -->
                            <div class="col-md-12">
                                <div class="news-item">
                                    <div class="img">
                                        <a href="#"><img src="public/layout/images/home/lotusnews/img-2.jpg" alt=""></a>
                                    </div>
                                    <div class="text">
                                        <span class="date">8 / March</span>
                                        <h2><a href="#">WEDDING DAY JONATHA &amp; JESSICA</a></h2>
                                        <a href="#" class="read-more">[ Read More ]</a>
                                    </div>
                                </div>
                            </div>
                            <!-- END / ITEM -->

                            <!-- ITEM -->
                            <div class="col-md-12">
                                <div class="news-item">
                                    <div class="img">
                                        <a href="#"><img src="public/layout/images/home/lotusnews/img-3.jpg" alt=""></a>
                                    </div>
                                    <div class="text">
                                        <span class="date">16 / February</span>
                                        <h2><a href="#">THE BEST WEDDING GUIDE 2015</a></h2>
                                        <a href="#" class="read-more">[ Read More ]</a>
                                    </div>
                                </div>
                            </div>
                            <!-- END / ITEM -->

                        </div>

                        <a href="#" class="awe-btn awe-btn-default">VIEW MORE</a>

                    </div>

                </div>
                <!-- END / NEWS -->

            </div>

            <div class="hr"></div>

        </div>

    </div>
</section>
<!-- END / HOME NEW -->