<!-- CHECK AVAILABILITY -->
<section class="section-check-availability">
    <div class="container">
        <div class="check-availability">
            <div class="row">
                <div class="col-lg-3">
                    <h2>CHECK AVAILABILITY</h2>
                </div>
                <div class="col-lg-9">
                    <form id="ajax-form-search-room"  method="post">
                        <div class="availability-form">
                            <input type="text" name="arrive" class="awe-calendar from" placeholder="Arrival Date">
                            <input type="text" name="departure" class="awe-calendar to" placeholder="Departure Date">
                            <?php
                            include ("models/m_room.php");
                            $m_room= new m_room();
                            $room = $m_room ->read_room();
                            ?>
                            <select class="awe-select" name="adults">
                                <?php foreach ($room as $key=> $value){ ?>
                                <option>Phòng</option>
                                <option>
                                    <?php echo $value->room_name?>
                                    <?php } ?>
                                </option>

                            </select>
                            <div class="vailability-submit">
                                <button  href="reservation_step2.php" class="awe-btn awe-btn-13">Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END / CHECK AVAILABILITY -->
